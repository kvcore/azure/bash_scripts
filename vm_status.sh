#!/bin/bash

#######################################################################
#Script to create custom Virtual Machine status tag in Azure dashboard#
#######################################################################

az login --service-principal -u 9f90391f-7459-46c6-88d8-f5af6a7d5e68 -p bf3aKDW6Ev7PZSYTHjCeKA3hF1jC~60b9y --tenant 993fb618-3f93-4557-8cb6-bc8acc5a238d

#Get a list of virtual machines
virtualMachines=($(az vm list --query [].name -o tsv))

#Loop over the list of machines
#---> For each machine:
#---> Get the instance status
#---> Get the resource id
#---> Update the status based on the vm state
for i in "${virtualMachines[@]}"
do
 export NAME=$i

 status=$(az vm get-instance-view --name $NAME --query instanceView.statuses[1].displayStatus)
 res_id=$(az vm get-instance-view --name $NAME --query id -o tsv)

 tag=$(az vm get-instance-view --name $NAME --query tags)



 case "$status" in
  '"VM deallocating"')
   if [ -z "$tag" ]
   then
        az tag create --resource-id $res_id --tags VM_Status="Powering Off"
   else
        az tag update --resource-id $res_id --operation replace --tags VM_Status="Powering Off"
   fi
 ;;
   '"VM deallocated"')
   if [ -z "$tag" ]
   then
        az tag create --resource-id $res_id --tags VM_Status=Off
   else
        az tag update --resource-id $res_id --operation replace --tags VM_Status=Off
   fi
 ;;
   '"VM running"')
   if [ -z "$tag" ]
   then
        az tag create --resource-id $res_id --tags VM_Status=On
   else
        az tag update --resource-id $res_id --operation replace --tags VM_Status=On
   fi
 ;;
       *)
     echo "DEFAULT"
 ;;
 esac

done
