#!/bin bash

echo Enter a name for the Azure SPN

read spn_name

az login --service-principal -u <username> -p <password> --tenant <tenant>

OUTPUT=$(az ad sp create-for-rbac --name $spn_name)

sp_appId=$(echo $OUTPUT | jq '.appId')

sp_displayName=$(echo $OUTPUT | jq '.displayName')

sp_name=$(echo $OUTPUT | jq '.name')

sp_password=$(echo $OUTPUT | jq '.password')

sp_tenant=$(echo $OUTPUT | jq '.tenant')

vault login
#or already be logged in vault

vault kv put secret/kvcore/azure/spn/$spn_name appId=$sp_appId displayName=$sp_displayName name=$sp_name password=$sp_password tenant=$sp_tenant 
